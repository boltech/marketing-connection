# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'marketing_connection/version'

Gem::Specification.new do |spec|
  spec.name          = "marketing_connection"
  spec.version       = MarketingConnection::VERSION
  spec.authors       = ["Erick"]
  spec.email         = ["erick_8911@hotmail.com"]
  spec.description   = %q{MArketing conneciton}
  spec.summary       = %q{MArketing connection}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
