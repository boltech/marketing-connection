module MarketingConnection
  class Delivery

#    def initailize(opts = {})
#
#    end

    def list(opts = {})
      MarketingConnection.submit(:get, delivery_url, opts)
    end

    def create(params = {})
      MarketingConnection.submit(:post, delivery_url, params)
    end

    private

      def delivery_url
        "deliveries"
      end

  end
end
