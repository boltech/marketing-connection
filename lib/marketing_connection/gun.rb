module MarketingConnection
  class Gun
    def initialize(opts = {})
    end

    def list(opts = {})
      MarketingConnection.submit(:get, gun_url, opts)
    end

    private
      def gun_url
        "guns"
      end
  end
end
